var AWS = require('aws-sdk');

exports.handler = async (event) => {
    // TODO implement
    
    var awsConfig = { 
    "accessKeyId": event.accessKeyId,
    "secretAccessKey": event.secretAccessKey,
    "region": event.region
    };
    
    var s3 = new AWS.S3(awsConfig);
    
    var params = {
      Bucket: event.bucket
    };
        
    try{
      var getObjects = await s3.listObjects(params).promise();
      var domain = 'http://d38wisknos2yyw.cloudfront.net/';
      getObjects.Contents.forEach(element => {
            element.ObjectUrl=domain+element.Key;
      });
      // console.log(getObjects.Contents);
      return getObjects.Contents;
    }
    catch(err){
      console.log(err);
    }
        
}